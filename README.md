# FSWAP

The File Swapper

## Basic Idea
Your laptop has limited space, your desktop and your server are less limited.

    Laptop:                      Server:           Desktop:
                                 ____________
                                |         =  |
         ___________            |  _________ |
        | --------- |           |  _________ |      _______
        | | small | |           |  Enormous_ |     |     - |
        | | disk  | |           |  Disk_____ |     | _____ |
        |___________|           |  _________ |     | Huge  |
        /  =======  \           |            |     | Disk  |
       /_____==______\          |____________|     |_______|
       
But this poses a problem- how do you work on a lot of large projects on the laptop
if it doesn't have the disk space for all of them without losing track of things?

This is what fswap is for. It's built to allow for swapping files on multiple computers
with a master copy stored in a personal server.

## Installation
1. Check to see if you have a posix shell. This might not be super urgent, but I make no
   guarantees that any of this will work if /bin/sh points to bash rather than a real
   POSIX shell, like dash. Debian, for example, is already set up correctly. Fedora and
   Arch are not.
2. Check to make sure that you have rsync and/or git installed. If you don't intend to use
   one or the other with this script, this doesn't matter as much.
3. Make sure you have a directory you can write to in your path. Most people use ~/bin, I
   highly recommend this.
4. Place fswap in your bin directory.
5. Create two symlinks to fswap, one called fswapin and one called fswapout.

## Workflow
The workflow with this script goes something like this:

1. Create a directory on the server for a new project. (The script can do
   this) Once the directory is created, we don't need to do this step again.
2. Pull the directory we want to work on to the laptop.
3. While working, periodically run the fswapin command on the current
   directory to push back the changes we've made
4. When we're done with the project, cd one directory up, then run fswapout
   on the directory to upload our changes and free up the disk space that
   project was taking up.
